<?php
declare(strict_types=1);

use App\Http\Controllers\ApiV1\RateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'ApiV1', 'middleware' => 'jwt'], function () {
    Route::get('current', [RateController::class, 'current']);
    Route::get('history', [RateController::class, 'history']);
});
