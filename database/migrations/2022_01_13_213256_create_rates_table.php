<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ru_rates', function (Blueprint $table) {
            $table->id();
            $table->integer('num_code')->nullable();
            $table->string('char_code')->nullable();
            $table->integer('nominal')->nullable();
            $table->string('name')->nullable();
            $table->double('value')->nullable();
            $table->double('previous')->nullable();
            $table->dateTimeTz('date');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ru_rates');
    }
}
