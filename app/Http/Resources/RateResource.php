<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\RuRate;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     description="Ресурс курса валюты",
 *     @OA\Property(
 *         property="num_code",
 *         type="integer",
 *         example="36",
 *         description="Код"
 *     ),
 *     @OA\Property(
 *         property="char_code",
 *         type="string",
 *         example="AUD",
 *         description="Символьный код"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         example="Австралийский доллар",
 *         description="Название"
 *     ),
 *     @OA\Property(
 *         property="value",
 *         type="float",
 *         example="54.5022",
 *         description="Значение"
 *     ),
 *     @OA\Property(
 *         property="previous",
 *         type="float",
 *         example="54.5022",
 *         description="Предыдущее значение"
 *     ),
 *     @OA\Property(
 *         property="date",
 *         type="string",
 *         example="2022-01-14 08:30:00+00",
 *         description="Дата"
 *     ),
 * )
 */

class RateResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var RuRate|self $this */
        return [
            'num_code'  => $this->num_code,
            'char_code' => $this->char_code,
            'nominal'   => $this->nominal,
            'name'      => $this->name,
            'value'     => $this->value,
            'previous'  => $this->previous,
            'date'      => $this->date,
        ];
    }
}
