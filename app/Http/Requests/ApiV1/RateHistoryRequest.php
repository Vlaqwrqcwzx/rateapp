<?php
declare(strict_types=1);

namespace App\Http\Requests\ApiV1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     description="История изменения курса валюты",
 *     @OA\Property(
 *         property="date",
 *         type="string",
 *         example="2020-04-20",
 *         pattern="Y-m-d",
 *         description="Дата",
 *     ),
 * )
 */
class RateHistoryRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'date' => ['required', 'date_format:Y-m-d'],
        ];
    }
}
