<?php
declare(strict_types=1);

namespace App\Http\Controllers\ApiV1;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Resources\RateResource;
use App\Http\Requests\ApiV1\RateHistoryRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class RateController extends Controller
{
    /**
     * @OA\Get(
     *     summary="Текущий курс валюты ru",
     *     path="/current",
     *     description="Текущий курс валюты ru",
     *     tags={"Rate"},
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             allOf={
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         @OA\Items(ref="#/components/schemas/RateResource")
     *                     ),
     *                 ),
     *                 @OA\Schema(ref="#/components/schemas/PaginatedResourceResponse")
     *             }
     *         ),
     *     ),
     * )
     */
    public function current(): AnonymousResourceCollection
    {
        $models = DB::table('ru_rates')->whereDate('date', Carbon::today())->get();
        return RateResource::collection($models);
    }

    /**
     * @OA\Get(
     *     summary="История валюты ru",
     *     path="/history",
     *     description="Текущий курс валюты ru",
     *     tags={"Rate"},
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             allOf={
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         @OA\Items(ref="#/components/schemas/RateResource")
     *                     ),
     *                 ),
     *                 @OA\Schema(ref="#/components/schemas/PaginatedResourceResponse")
     *             }
     *         ),
     *     ),
     * )
     * @param RateHistoryRequest $request
     * @return AnonymousResourceCollection
     */
    public function history(RateHistoryRequest $request): AnonymousResourceCollection
    {
        $date = $request->get('date');
        $models = DB::table('ru_rates')->whereDate('date', $date)->get();
        return RateResource::collection($models);
    }
}
