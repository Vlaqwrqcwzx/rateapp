<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Firebase\JWT\JWT;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JwtAuth
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key = env('JWT_SECRET', '');

        $token = $request->headers->get('X-Auth-Token');

        if (!$token) {
            abort(422, 'X-Auth-Token required!');
        }

        $payload = JWT::decode($token, $key, ['HS256']);

        if (!isset($payload->guid)) {
            abort(422, 'guid required!');
        }

        if (!Uuid::isValid($payload->guid)) {
            abort(422, 'guid not valid!');
        }

        $user = new User(['id' => $payload->guid]);
        Auth::setUser($user);

        return $next($request);
    }
}
