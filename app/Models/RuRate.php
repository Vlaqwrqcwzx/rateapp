<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RuRate
 *
 * @property int $id
 * @property int|null $num_code
 * @property string|null $char_code
 * @property int|null $nominal
 * @property string|null $name
 * @property float|null $value
 * @property float|null $previous
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereCharCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereNominal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereNumCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate wherePrevious($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RuRate whereValue($value)
 * @mixin \Eloquent
 */
class RuRate extends Model
{
    protected $fillable = [
        'num_code',
        'char_code',
        'nominal',
        'name',
        'value',
        'previous',
        'date',
    ];
}
