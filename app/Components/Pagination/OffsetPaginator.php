<?php
declare(strict_types=1);

namespace App\Components\Pagination;

use Closure;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Collection;

class OffsetPaginator extends AbstractPaginator
{
    /**
     * Total number of items
     *
     * @var int
     */
    protected $total;

    /**
     * The number of items to be shown.
     *
     * @var int
     */
    protected $limit;

    /**
     * The current offset being "viewed".
     *
     * @var int
     */
    protected $offset;

    /**
     * The query string variable used to store the limit.
     *
     * @var string
     */
    protected $limitName = 'limit';

    /**
     * The query string variable used to store the offset.
     *
     * @var string
     */
    protected $offsetName = 'offset';

    /**
     * The current limit resolver callback.
     *
     * @var Closure
     */
    protected static $currentLimitResolver;

    /**
     * The current offset resolver callback.
     *
     * @var Closure
     */
    protected static $currentOffsetResolver;

    public function __construct($items, int $total, int $limit = null, int $offset = null, array $options = [])
    {
        $this->options = $options;

        foreach ($options as $key => $value) {
            $this->{$key} = $value;
        }

        $this->total = $total;
        $this->limit = $this->setCurrentLimit($limit);
        $this->offset = $this->setCurrentOffset($offset);

        $this->setItems($items);
    }

    public function limit(): int
    {
        return $this->limit;
    }

    public function offset(): int
    {
        return $this->offset;
    }

    public function total(): int
    {
        return $this->total;
    }

    public function toArray(): array
    {
        return [
            'data' => $this->items->toArray(),
            'limit' => $this->limit(),
            'offset' => $this->offset(),
            'total' => $this->total(),
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    protected function setCurrentLimit(int $currentLimit): int
    {
        $currentLimit = $currentLimit ?: static::resolveCurrentLimit();

        return $this->isValidNumber($currentLimit) ? $currentLimit : 10;
    }

    protected function setCurrentOffset(int $currentOffset): int
    {
        $currentOffset = $currentOffset ?: static::resolveCurrentOffset();

        return $this->isValidNumber($currentOffset) ? $currentOffset : 0;
    }

    protected function setItems($items)
    {
        $this->items = $items instanceof Collection ? $items : Collection::make($items);

        $this->items = $this->items->slice(0, $this->limit);
    }

    public static function isValidNumber(int $limit): bool
    {
        return $limit >= 0 && filter_var($limit, FILTER_VALIDATE_INT) !== false;
    }

    public static function resolveCurrentLimit(string $limitName = 'limit', int $default = 50): int
    {
        if (isset(static::$currentLimitResolver)) {
            return (int)call_user_func(static::$currentLimitResolver, $limitName, $default);
        }

        return $default;
    }

    public static function resolveCurrentOffset(string $offsetName = 'offset', int $default = 0): int
    {
        if (isset(static::$currentOffsetResolver)) {
            return (int)call_user_func(static::$currentOffsetResolver, $offsetName, $default);
        }

        return $default;
    }

    public static function currentLimitResolver(Closure $resolver)
    {
        static::$currentLimitResolver = $resolver;
    }

    public static function currentOffsetResolver(Closure $resolver)
    {
        static::$currentOffsetResolver = $resolver;
    }
}
