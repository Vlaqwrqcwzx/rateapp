<?php
declare(strict_types=1);

namespace App\Components\Pagination;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse as BasePaginatedResourceResponse;

/**
 * @OA\Schema(
 *     description="Ресурс коллекции",
 *     @OA\Property(
 *         property="meta",
 *         type="object",
 *         @OA\Property(
 *             property="limit",
 *             type="integer",
 *             description="Кол-во элементов на странице",
 *         ),
 *         @OA\Property(
 *             property="offset",
 *             type="integer",
 *             description="Какой сдвиг сделан",
 *         ),
 *         @OA\Property(
 *             property="total",
 *             type="integer",
 *             description="Всего элементов в списке",
 *         ),
 *     )
 * )
 */
class PaginatedResourceResponse extends BasePaginatedResourceResponse
{
    /**
     * Add the pagination information to the response.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function paginationInformation($request)
    {
        $paginated = $this->resource->resource->toArray();

        return [
            'meta' => $this->meta($paginated),
        ];
    }
}
