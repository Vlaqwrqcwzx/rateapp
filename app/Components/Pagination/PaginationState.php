<?php
declare(strict_types=1);

namespace App\Components\Pagination;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class PaginationState
{
    /**
     * Bind the pagination state resolvers using the given application container.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    public static function resolveUsing($app)
    {
        OffsetPaginator::currentLimitResolver(function (string $limitName = 'limit', int $default = 50) use ($app): int {
            $limit = (int)$app['request']->input($limitName, $default);
            return OffsetPaginator::isValidNumber($limit) ? $limit : $default;
        });

        OffsetPaginator::currentOffsetResolver(function (string $offsetName = 'offset', int $default = 0) use ($app): int {
            $offset = (int)$app['request']->input($offsetName, $default);
            return OffsetPaginator::isValidNumber($offset) ? $offset : $default;
        });

        $macro = function (int $limit = null, array $columns = ['*'], string $limitName = 'limit', string $offsetName = 'offset', int $offset = null, array $options = []) {
            $limit = $limit ?: OffsetPaginator::resolveCurrentLimit($limitName, $this->model->getPerPage());
            $limit = $limit > 100 ? 100 : $limit;

            $offset = $offset ?: OffsetPaginator::resolveCurrentOffset($offsetName);

            $results = ($total = $this->toBase()->getCountForPagination())
                ? $this->offset($offset)->limit($limit)->get($columns)
                : $this->model->newCollection();

            return new OffsetPaginator($results, $total, $limit, $offset, $options);
        };

        QueryBuilder::macro('offsetPaginate', $macro);
        EloquentBuilder::macro('offsetPaginate', $macro);
    }
}
