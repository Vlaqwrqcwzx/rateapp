<?php
declare(strict_types=1);

namespace App\Components\Pagination;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection as BaseAnonymousResourceCollection;

class AnonymousResourceCollection extends BaseAnonymousResourceCollection
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        if ($this->resource instanceof OffsetPaginator) {
            return (new PaginatedResourceResponse($this))->toResponse($request);
        }

        return parent::toResponse($request);
    }
}
