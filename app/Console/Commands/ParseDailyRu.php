<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\RuRate;
use Illuminate\Console\Command;

class ParseDailyRu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse-daily-ru';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \JsonException
     */
    public function handle()
    {
        $rates = json_decode(file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js'), true, 512, JSON_THROW_ON_ERROR);

        foreach ($rates['Valute'] as $code => $item) {
            RuRate::updateOrCreate([
                'date'      => $rates['Date'],
                'num_code'  => $item['NumCode'],
            ], [
                'num_code'  => $item['NumCode'],
                'char_code' => $item['CharCode'],
                'nominal'   => $item['Nominal'],
                'name'      => $item['Name'],
                'value'     => $item['Value'],
                'previous'  => $item['Previous'],
            ]);
        }

        return 0;
    }
}
